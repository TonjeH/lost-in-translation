import { API_URL, API_KEY } from "../config";

export async function getUser(username) {
  try {
    console.log("Fetching users....");
    return await fetch(`${API_URL}?username=${username}`)
    .then(res => res.json());
  } catch(err) {
    console.log("Error: ", err.message);
  }
}

export async function createUser(username, translations) {
  try{
    return await fetch(`${API_URL}`, {
      method: 'POST',
      headers: {
        'X-API-Key': API_KEY,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ 
          username: username, 
          translations: translations 
      })
    })
    .then(res => {
      if (!res.ok) {
        throw new Error('Could not create new user');
      }
      return res.json()
    })
  } catch(err) {
    console.log("Error: ", err.message);
  }
}

export async function updateUser(userId, translations) {
  try{ return await 
    fetch(`${API_URL}/${userId}`, {
      method: 'PATCH',
      headers: {
        'X-API-Key': API_KEY,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
          translations: translations 
      })
  })
  .then(response => {
    if (!response.ok) {
      throw new Error('Could not update translations history')
    }
    return response.json()
  })
  } catch(err) {
    console.log("Error: ", err.message);
  }
}
import { Link } from "react-router-dom";
import styles from '../styles/Header.module.css';

export default function Header() {
  return(
    <header>
      <Link to='/'>
      <h1 className={styles.title}>Lost in Translation</h1>
      </Link>
      
    </header>
  )
};

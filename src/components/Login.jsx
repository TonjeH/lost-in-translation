import styles from '../styles/Login.module.css';
import { useUser } from "../contexts/UserContext";
import { useHistory } from "react-router";
import { getUser, createUser } from '../api/lostInTranslationAPI';

export default function Login() {

  const { user, setUser } = useUser()
  const history = useHistory()
  
  async function handleLoginClick(e) {
    e.preventDefault();
    const current = await getUser(user);
    if(current && current.length === 0) {
      console.log("Creating user");
      createUser(user, []);
    }
    if(user !== '') history.push('/translate');
  }

  function onUsernameChange(e) {
    setUser(e.target.value)
  }

  return(
    <>
      <label>
        Username:    
      </label>
      <input type="text" value={ user } placeholder="Username" className={styles.input} onChange={onUsernameChange} />
      <button type="submit" onClick={ handleLoginClick } className={styles.button}>🠖</button>
    </>
  )
};

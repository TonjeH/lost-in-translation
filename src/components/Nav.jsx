import { Link } from "react-router-dom";
import styles from '../styles/Nav.module.css';

export default function Nav() {
  return(
    <nav className={styles.nav}>
      <ul className={styles.ul}>
        <li className={styles.li}>
          <Link to="/">Home</Link>
        </li>
        <li className={styles.li}>
          <Link to="/translate">Translate</Link>
        </li>
        <li className={styles.li}>
          <Link to="/profile">Profile</Link>
        </li>
      </ul>
    </nav>
  )
};

import Login from "./Login";
import styles from '../styles/Startup.module.css';

export default function Startup() {
  return(
    <section className={styles.section}>
      <form className={styles.form}>
        <Login />
      </form>
    </section>
    
  )
};

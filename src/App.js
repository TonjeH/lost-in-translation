import {
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom';

import Header from './components/Header';
import Nav from './components/Nav';
import Startup from './components/Startup';
import Translate from './components/Translate';
import Profile from './components/Profile';
import './styles/App.css';

export default function App() {
  return (
    <Router>
      <div className='App'>
        <Header />
        <Nav />
        <main>
          <Switch>
            <Route path = '/translate' component={ Translate }/>
            <Route path = '/profile' component={ Profile } />
            <Route path = '/' exact component={ Startup } />
          </Switch>
        </main>
      </div>
    </Router>
  );
}

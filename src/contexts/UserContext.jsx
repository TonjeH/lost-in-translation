import { createContext, useContext, useState } from "react";

const UserContext = createContext('');

export default function UserProvider({ children }) {
  const [user, setUser] = useState('');
  // const value = [user, setUser];
  return <UserContext.Provider value={{ user, setUser }}>
    {children}
  </UserContext.Provider>;
};

export function useUser() {
  const context = useContext(UserContext);
  if(!context) {
    throw new Error("useUser must be used within a UserProvider");
  }
  return context;
}



import { createContext, useContext, useReducer } from "react";

const TranslationContext = createContext(null);

export function useTranslationContext() {
  return useContext(TranslationContext);
}

function translationReducer(state, action) {
  switch(action.type) {
    case 'SET_TRANSLATIONS':
      return {
        loading: false,
        error: '',
        translations: action.payload
      }
    case 'SET_TRANSLATIONS_ERROR':
      return {
        ...state,
        loading: false,
        error: action.payload
      }
    default: return state
  }
}

const initialState = {
  loading: true,
  error: '',
  translations: []
}

export default function TranslationProvider({ children }) {
  const [ translationState, dispatch ] = useReducer(translationReducer, initialState);

  return(
    <TranslationContext.Provider value={{ translationState, dispatch }}>
      {children}
    </TranslationContext.Provider>
  )
  
};

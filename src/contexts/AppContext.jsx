import TranslationProvider from "./TranslationContext";
import UserProvider from "./UserContext";

export default function AppContext({ children }) {
  return(
    <UserProvider>
      <TranslationProvider>
        {children}
      </TranslationProvider>
    </UserProvider>
  )  
};
